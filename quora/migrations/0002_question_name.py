# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-10 09:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quora', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='name',
            field=models.CharField(blank=True, default='Empty', max_length=255, null=True),
        ),
    ]
