# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-08 10:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Deal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('sort_order', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='deal',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='deals.Status'),
        ),
    ]
