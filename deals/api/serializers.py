from rest_framework.serializers import ModelSerializer

from deals.models import Deal, History

class DealCreateUpdateSerializer(ModelSerializer):
	class Meta:
		model = Deal
		fields = [
			'name',
			'price',
			'status',
		] 

class DealListSerializer(ModelSerializer):
	class Meta:
		model = Deal
		fields = [
			'name',
			'price',
			'status',
		] 

class DealDetailSerializer(ModelSerializer):
	class Meta:
		model = Deal
		fields = [
			'name',
			'price',
			'status',
		] 

class HistoryListSerializer(ModelSerializer):
	class Meta:
		model = History
		fields = [
			'deal_id',
			'status',
			'started',
			'ended',
		] 