from django.db import models
from model_utils import FieldTracker


class Question(models.Model):
	body = models.CharField(max_length=255)
	answers = models.PositiveIntegerField(default=0, blank=True, null=True)
	name = models.CharField(max_length=255, default="Empty", blank=True, null=True)

	def __str__(self):
		return self.body

class Answer(models.Model):
	ques = models.ForeignKey('Question')
	body = models.CharField(max_length=255)
	#created = models.DateTimeField(auto_now=True)
	#updated = models.DateTimeField(auto_now=False, default=timezone.now)

	tracker = FieldTracker()

	def __str__(self):
		return self.body

class History(models.Model):
	question = models.CharField(max_length=255)
	isanswer = models.CharField(max_length=255)

	def __str__(self):
		return self.question