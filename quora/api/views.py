from rest_framework.generics import (
	CreateAPIView,
	DestroyAPIView,
	ListAPIView,
	RetrieveUpdateAPIView,
	RetrieveAPIView,
	)

from quora.models import Answer, History

from .serializers import (
	AnswerCreateUpdateSerializer,
	AnswerListSerializer,
	QuestionListSerializer,
	AnswerDetailSerializer,
	)

class AnswerCreateAPIView(CreateAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerCreateUpdateSerializer

class AnswerDetailAPIView(RetrieveAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerDetailSerializer

class AnswerUpdateAPIView(RetrieveUpdateAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerCreateUpdateSerializer

class AnswerDeleteAPIView(DestroyAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerDetailSerializer

class AnswerListAPIView(ListAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerListSerializer

class QuestionListAPIView(ListAPIView):
	queryset = History.objects.all()
	serializer_class = QuestionListSerializer
