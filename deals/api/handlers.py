from django.db.models.signals import post_save
from django.dispatch import receiver
from model_utils import FieldTracker
from django.utils import timezone
import datetime

from deals.models import Deal, History

@receiver(post_save, sender=Deal)
def add_ans_count(instance, created, **kwargs):
	if created:
		p = History(deal_id = instance.id, status = instance.status.name, started = instance.created, ended = '2010-10-10 00:00')
		p.save()
	if instance.has_changed and 'status' in instance.changed_fields:
		t = History.objects.filter(deal_id=instance.id).latest('started')
		if t:
			t.ended = datetime.datetime.now()
			t.save()
			p = History(deal_id = instance.id, status = instance.status.name, started = datetime.datetime.now(), ended = '2010-10-10 00:00')
			p.save()
		else:
			p = History(deal_id = instance.id, status = instance.status.name, started = datetime.datetime.now(), ended = '2010-10-10 00:00')
			p.save()

	#instance.ques.answers += 1
		#instance.ques.name = instance.tracker.previous('body')