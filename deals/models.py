from django.db import models
from django.utils import timezone
from model_utils import FieldTracker

from django.forms.models import model_to_dict


class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in
                             self._meta.fields])

class Status(models.Model):
	name = models.CharField(max_length=255)
	sort_order = models.IntegerField()

	def __str__(self):
		return self.name

class Deal(models.Model, ModelDiffMixin):
	status = models.ForeignKey(Status, on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	price = models.FloatField()
	created = models.DateTimeField(default=timezone.now)
	updated = models.DateTimeField(auto_now=False, blank=True, null=True)

	tracker = FieldTracker()

	def __str__(self):
		return self.name

class History(models.Model):
	deal_id = models.IntegerField()
	status = models.CharField(max_length=255, null=True)
	started = models.DateTimeField(default=timezone.now)
	ended = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.status