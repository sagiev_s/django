from django.conf.urls import url
from django.contrib import admin

from .views import (
	AnswerCreateAPIView,
	AnswerListAPIView,
	QuestionListAPIView,
	AnswerDetailAPIView,
	AnswerUpdateAPIView,
	AnswerDeleteAPIView,
	)

urlpatterns = [
	url(r'^$', AnswerListAPIView.as_view(), name='list'),
	url(r'^history/$', QuestionListAPIView.as_view(), name='history'),
	url(r'^create/$', AnswerCreateAPIView.as_view(), name='create'),
	url(r'^(?P<pk>\d+)/$', AnswerDetailAPIView.as_view(), name='detail'),
	url(r'^(?P<pk>\d+)/edit/$', AnswerUpdateAPIView.as_view(), name='update'),
	url(r'^(?P<pk>\d+)/delete/$', AnswerDeleteAPIView.as_view(), name='delete'),
]