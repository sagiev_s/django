from django.db.models.signals import post_save
from model_utils import FieldTracker
from django.dispatch import receiver

from quora.models import Answer, Question, History

@receiver(post_save, sender=Answer)
def add_ans_count(instance, created, **kwargs):
	instance.ques.answers += 1
	p = History(question = instance.ques.body, isanswer = instance.tracker.previous('ques'))
	p.save()
	#instance.ques.name = instance.tracker.previous('body')
	instance.ques.save()