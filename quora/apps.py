from django.apps import AppConfig


class QuoraConfig(AppConfig):
    name = 'quora'

    def ready(self):
    	import quora.api.handlers
