from django.contrib import admin
from .models import Deal, Status, History

admin.site.register(Deal)
admin.site.register(Status)
admin.site.register(History)