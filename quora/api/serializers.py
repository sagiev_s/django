from rest_framework.serializers import ModelSerializer

from quora.models import Answer, History

class AnswerCreateUpdateSerializer(ModelSerializer):
	class Meta:
		model = Answer
		fields = [ 
			'body',
			'ques',
		] 

class AnswerListSerializer(ModelSerializer):
	class Meta:
		model = Answer
		fields = [
			'id',
			'body',
			'ques',
		] 

class QuestionListSerializer(ModelSerializer):
	class Meta:
		model = History
		fields = [
			'question',
			'isanswer',
		] 

class AnswerDetailSerializer(ModelSerializer):
	class Meta:
		model = Answer
		fields = [
			'id',
			'body',
			'ques',
		] 